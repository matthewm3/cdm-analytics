drop index idx_collection_site_alias;
drop index idx_vocabulary_name;

drop table vocabulary_term;
drop table if exists vocabulary;
drop table if exists vocabulary_type;
drop table if exists metadata_field;
drop table if exists collection;
drop table if exists site;

--
-- Site table.
--
-- Mostly used to maintain referential integrity.
--
create table site
(
    site_id          varchar(25) not null primary key,
    created_at       timestamp   not null
);
comment on column site.created_at is 'Date site was loaded.';

--
-- Collections in a site.
--
create table collection
(
    collection_id uuid         not null primary key,
    alias         varchar(200) not null,
    description   varchar(512) not null,
    path          varchar(500) not null,
    site_id       varchar(25)  not null references site (site_id) on delete cascade,
    unique (site_id, alias)
);
comment on table collection is 'Collection data loaded from $ANALYTICS_DATA_PATH/sites/{site}/server/docs/catalog.txt';

create index idx_collection_site_alias on collection (site_id, alias);

create table metadata_field
(
    metadata_field_id uuid         not null primary key,
    field_name        varchar(100) not null,
    nickname          varchar(100) not null,
    data_type         varchar(50),
    large_field       varchar(25),
    required          bool         not null default false,
    searchable        boolean      not null default false,
    hidden            boolean      not null default false,
    vocabulary        varchar(25)  not null,
    dc_map            varchar(25)  not null,
    collection_id     uuid         not null references collection (collection_id) on delete cascade,
    unique (collection_id, field_name)
);
comment on table metadata_field is 'Site''s metadata field configuration from $ANALYTICS_DATA_PATH/sites/{site}/data/{collection}/index/etc/config.txt';

--
-- Allowed vocabulary types.
--
create table vocabulary_type
(
    code        char(1)     not null primary key,
    description varchar(25) not null
);

---
--- List of all vocabularies.
---
create table vocabulary
(
    vocabulary_id   uuid        not null primary key,
    name            varchar(25) not null,
    description     varchar(250),
    vocabulary_type char(1)     not null references vocabulary_type (code),
    site_id         varchar(25) not null references site (site_id) on delete cascade,
    unique (site_id, name)
);
comment on column vocabulary.name is 'Short name identifying the vocabulary (e.g. AAT, MESH, 1, etc)';
comment on column vocabulary.description is 'Vocabulary description (e.g. Newspapers).';

create index idx_vocabulary_name on vocabulary (name);

--
-- List of terms in a vocabulary
--
create table vocabulary_term
(
    vocabulary_id uuid references vocabulary (vocabulary_id) on delete cascade,
    term          varchar(250) not null
    -- Ideal, but the file data allows duplicates.
    -- unique(vocabulary_id, term)
);

insert into vocabulary_type
values ('a', 'Authority'),
       ('s', 'Shared'),
       ('c', 'Collection')
;

