# ContentDM Analytics

Extracts site configuration from text files and loads to a Postgres SQL database for further analysis.

## Running

For development/testing, you can run the following commands using Poetry:

```shell
poetry run cdm-export export 17125
```
```shell
poetry run cdm-export delete 17125
```

## Publishing
To publish the package to OCLC's local PyPi repository, you need to configure Poetry.

``` shell
poetry config http-basic.pypi-local my_pypi_publisher eyJ2ZXIiOiIyIiwidHlwIjoiSldUIiwiYWxnIjoiUlMyNTYiLCJraWQiOiI5RzRrOWdmVXRDd3g1bFB4RjhGb19NRTZteHdCcXNGRk1yT0JaRHd6eHgwIn0.eyJzdWIiOiJqZnJ0QDAxYzRoejI2NXBtMWg2MWN3cDdxOHMwZzQ4XC91c2Vyc1wvbTFfcHlwaV9wdWJsaXNoZXIiLCJzY3AiOiJtZW1iZXItb2YtZ3JvdXBzOnJlYWRlcnMscHlwaS1wdWJsaXNoZXJzIGFwaToqIiwiYXVkIjoiamZydEAwMWM0aHoyNjVwbTFoNjFjd3A3cThzMGc0OCIsImlzcyI6ImpmcnRAMDFjNGh6MjY1cG0xaDYxY3dwN3E4czBnNDgiLCJpYXQiOjE2MzM0NjEyMzQsImp0aSI6ImVkMmJmYjgyLWFhZGEtNGExMy1hMDJmLWM5ZThiZjliNWM3NiJ9.lvxBsvhJckjU9piu63q-aRPiDXOc-pwuyVk5FU7ddoS8QsMBwKo-2VGRF8JntVWIOU1UIToPC3AflxeFdRc9Y7kEagC43i_9p7aRY8Yu3-wRn_64tar5MgrLG86FGJhHnNx1iAWvvtNRLY-CMBdN-CG2woFWe1Ok0w_syVDm8YlNtrLZT7QiM0rKo_1nQVAxqddsJd0cVpntRPBs1drbgRjnXqwteORyNdWFSWbT4qU3BGZo_KvhALfDxaIESl6KL4_jbRKm6G-KkqvUYQwWPYdjeLvSZFi8pbNuoeadX55jd7RW07bO_9pWGiz5qak_aPmuqUCGrebfrQOGzDi8KA
```

This should update the following Poetry configuration files. 

```toml
# ~/Library/Application\ Support/pypoetry/config.toml   
[repositories.pypi-local]
url = "https://artifact-m1.shr.oclc.org/artifactory/api/pypi/pypi-local"

```

``` toml
# ~/Library/Application\ Support/pypoetry/auth.toml
[http-basic.pypi-local]
username = "m1_pypi_publisher"
password = "eyJ2ZXIiOiIyIiwidHlwIjoiSldUIiwiYWxnIjoiUlMyNTYiLCJraWQiOiI5RzRrOWdmVXRDd3g1bFB4RjhGb19NRTZteHdCcXNGRk1yT0JaRHd6eHgwIn0.eyJzdWIiOiJqZnJ0QDAxYzRoejI2NXBtMWg2MWN3cDdxOHMwZzQ4XC91c2Vyc1wvbTFfcHlwaV9wdWJsaXNoZXIiLCJzY3AiOiJtZW1iZXItb2YtZ3JvdXBzOnJlYWRlcnMscHlwaS1wdWJsaXNoZXJzIGFwaToqIiwiYXVkIjoiamZydEAwMWM0aHoyNjVwbTFoNjFjd3A3cThzMGc0OCIsImlzcyI6ImpmcnRAMDFjNGh6MjY1cG0xaDYxY3dwN3E4czBnNDgiLCJpYXQiOjE2MzM0NjEyMzQsImp0aSI6ImVkMmJmYjgyLWFhZGEtNGExMy1hMDJmLWM5ZThiZjliNWM3NiJ9.lvxBsvhJckjU9piu63q-aRPiDXOc-pwuyVk5FU7ddoS8QsMBwKo-2VGRF8JntVWIOU1UIToPC3AflxeFdRc9Y7kEagC43i_9p7aRY8Yu3-wRn_64tar5MgrLG86FGJhHnNx1iAWvvtNRLY-CMBdN-CG2woFWe1Ok0w_syVDm8YlNtrLZT7QiM0rKo_1nQVAxqddsJd0cVpntRPBs1drbgRjnXqwteORyNdWFSWbT4qU3BGZo_KvhALfDxaIESl6KL4_jbRKm6G-KkqvUYQwWPYdjeLvSZFi8pbNuoeadX55jd7RW07bO_9pWGiz5qak_aPmuqUCGrebfrQOGzDi8KA"
```

You can then use the following command to publish to the local PyPi repository.

``` shell
 poetry publish -r pypi-local
```


## Vocabularies
There are 3 different types of vocabularies. 

<dl>
  <dt><strong>Authority</strong></dt>
  <dd>
    Sometimes known as a "thesaurus" on the filesystem. This is a vocabulary maintained by a trusted authority. These are listed in the file `$CONTENTDMDATA/sites/{site}/Server/config/vocab/oclc.txt`. The terms for each thesaurus is listed in 
    the file `$CONTENTDMDATA/sites/{site}/Server/config/vocab/{vocabulary-name}.txt`
  </dd>
  <dt><strong>Shared vocabulary</strong></dt>
  <dd>
    This is a vocabulary that is either shared by fields in the same collection or by fields in multiple collections.
    A shared vocabulary is stored in the directory `$CONTENTDMDATA/sites/{site}/Server/config/vocab/`.  

    When a collection's field uses a custom vocabulary, a `.map` file extension is used instead of the customary `.txt`.  
  
    CAUTION: When both a .txt and .map vocabulary exist in `$CONTENTDMDATA/sites/{site}/data/{collection}/index/vocab`,
      the .txt file should be ignored.  The .map file will point to the shared vocabulary file in 
      `$CONTENTDMDATA/sites/{site}/Server/config/vocab/`.
  </dd>
  <dt><strong>Collection vocabulary</strong></dt>
  <dd>
    Vocabulary that is specific to a collection. Can be based on an authority, another vocabulary in the collection, 
    or the current values used by collection items. 
  </dd>
</dl>

