import os
import logging
import argparse
from uuid import uuid4

from dotenv import load_dotenv

from analytics_utils import db
from analytics_utils import sites as cdmdata

logger = logging.getLogger(__name__)

def extract_vocabulary_data(site: str):
    logger.info("Extracting site's authorities")
    authorities = cdmdata.list_authorities(site)

    logger.info("Extracting site's shared vocabularies")
    shared_vocab = cdmdata.list_shared_vocabularies(site)

    site_vocab = authorities + shared_vocab

    site_vocab_data = []
    vocab_terms_data = []
    for vocab in site_vocab:
        vocab['id'] = str(uuid4())
        site_vocab_data.append((
            vocab['id'],
            vocab['name'],
            vocab['description'],
            vocab['type'],
            site
        ))

        for term in vocab['terms']:
            vocab_terms_data.append((vocab['id'], term))

    cursor = connection.cursor()
    db.insert_vocabularies(cursor, site_vocab_data)
    db.insert_vocabulary_terms(cursor, vocab_terms_data)


def extract_site_data(site: str):
    """
    Extract site data.

    site -- non-null site
    """
    logger.info(f'Extracting data for site {site}')

    cursor = connection.cursor()
    db.create_site(cursor, site)

    extract_vocabulary_data(site)

    collections = cdmdata.list_collections(site)
    for collection in collections:
        collection_data = []
        metadata_field_data = []
        collection_vocab_data = []
        collection_vocabulary_list_data = []

        collection_alias = collection['alias'] = collection['alias'][1:]
        collection_id = str(uuid4())

        collection_data.append((collection_id, collection_alias, collection['description'], collection['path'], site))
        metadata_fields = cdmdata.list_metadata_fields(site, collection_alias)
        for metadata_field in metadata_fields:

            metadata_field_id = str(uuid4());
            # The metadata field's nickname.
            nickname = metadata_field['nickname']

            metadata_field_data.append((
                metadata_field_id,
                metadata_field['fieldName'],
                nickname,
                metadata_field['dataType'],
                metadata_field['largeField'],
                metadata_field['required'],
                metadata_field['searchable'],
                metadata_field['hidden'],
                metadata_field['vocabulary'],
                metadata_field['dcmap'],
                collection_id
            ))

            # if the metadata field has a vocabulary, extract it.
            if metadata_field['vocabulary'] != 'NOVOCAB':

                logger.debug('Extracting vocabulary for metadata field \'%s\' in site \'%s\', collection \'%s\'',
                             nickname,
                             site, collection_alias)

                if cdmdata.is_shared_vocabulary(site, collection_alias, nickname):
                    # Skip shared vocabularies as they've already been stored.
                    logger.debug('%s, %s, %s is a shared vocabulary. Skipping vocabulary import...', site,
                                 collection_alias, nickname)
                    continue

                logger.debug('Extracting vocabulary for collection');
                collection_vocabulary_id = str(uuid4())
                collection_vocab_data.append((
                    collection_vocabulary_id,
                    f'{site}/{collection_alias}/{nickname}',
                    f'Custom vocabulary for site {site}, collection {collection_alias}, and metadata field \'{nickname}\'.',
                    'c',
                    site
                ))

                terms = cdmdata.list_vocabulary_terms(site, collection_alias, nickname)
                for term in terms:
                    collection_vocabulary_list_data.append((collection_vocabulary_id, term))

        db.insert_collections(cursor, collection_data)
        db.insert_metadata_fields(cursor, metadata_field_data)
        db.insert_vocabularies(cursor, collection_vocab_data)
        db.insert_vocabulary_terms(cursor, collection_vocabulary_list_data)


def create_db_connection():
    """
    Create a db connection to which CDM data is exported.
    :return:
    """
    global connection
    connection = db.create_connection(os.environ['ANALYTICS_DB_NAME'],
                                      os.environ['ANALYTICS_DB_USER'],
                                      os.environ['ANALYTICS_DB_PASSWORD'],
                                      os.environ['ANALYTICS_DB_HOST'],
                                      os.environ['ANALYTICS_DB_PORT'])
    connection.autocommit = True


def export(args):
    """
    Execute the "export" sub-command.
    :param args: cli arguments
    """
    sites = args.site

    logger.info('Exporting data from %s to %s.',
                args.data_dir,
                f'SERVER={os.environ.get('ANALYTICS_DB_HOST')}:{os.environ.get('ANALYTICS_DB_PORT')};DATABASE={os.environ.get('ANALYTICS_DB_NAME')}')

    available_sites = cdmdata.list_sites()
    for site in sites:
        if site not in available_sites:
            logger.error('Site %s does not exist!', site)
            exit(-1)

    for site in sites:
        extract_site_data(site)


def delete(args):
    """
    Executes the "delete" sub-command.
    :param args: cli arguments
    """

    sites = args.site
    logger.debug('Deleting sites %s', sites)
    db.delete_sites(connection.cursor(), sites)

def run():
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(module)-20s %(message)s')

    load_dotenv()

    # create the top-level parser
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(required=True)

    # create the parser for the "export" command
    parser_export = subparsers.add_parser('export')
    parser_export.add_argument('site', help='Sites to export', default=None, nargs='+')
    parser_export.add_argument('--data-dir', help='Path to ContentDM data.', default=os.environ['ANALYTICS_DATA_PATH'])
    parser_export.set_defaults(func=export)

    # create the parser for the "delete" command
    parser_delete = subparsers.add_parser('delete')
    parser_delete.add_argument('site', help='Sites to delete', default=None, nargs='+')
    parser_delete.set_defaults(func=delete)
    args = parser.parse_args()

    create_db_connection()
    args.func(args)

if __name__ == '__main__':
    run()