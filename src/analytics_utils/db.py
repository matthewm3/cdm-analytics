"""
Utilities used to persist data to Postgres.
"""
import psycopg
import logging
from datetime import datetime
from psycopg import OperationalError

logger = logging.getLogger(__name__)

def create_connection(db_name: str, db_user: str, db_password: str, db_host: str, db_port: str):
    """
    Create a database connection to posgres

    :param db_name: database name
    :param db_user: user name
    :param db_password: password
    :param db_host: database host
    :param db_port: database port
    :return:
    """
    connection = None
    try:
        connection = psycopg.connect(
            dbname=db_name,
            user=db_user,
            password=db_password,
            host=db_host,
            port=db_port,
        )
        # logger.info("Connection to PostgreSQL DB successful")
    except OperationalError as e:
        logger.error(f"The error '{e}' occurred")
    return connection

def execute_query(connection, query: str):
    connection.autocommit = True
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        # logger.info("Query executed successfully")
    except OperationalError as e:
        logger.error(f"The error '{e}' occurred")

def create_site(cursor, site: str) :
    cursor.execute("insert into site(site_id, created_at) VALUES (%s, %s)", [site, datetime.now()])

def delete_sites(cursor, sites: str) :
    for site in sites:
        cursor.execute("delete from site where site_id = %(site)s;", {'site': site});

def insert_collections(cursor, collection_data: tuple):
    logging.info('Inserting %s collections', len(collection_data))
    with cursor.copy("COPY collection (collection_id, alias, description, path, site_id) FROM STDIN") as copy:
        for record in collection_data:
            copy.write_row(record)


def insert_metadata_fields(cursor, metadata_field_data: tuple):
    logging.info('Inserting %s metadata_field rows', len(metadata_field_data))
    with cursor.copy(
            "COPY metadata_field (metadata_field_id, field_name, nickname, data_type, large_field, required, searchable, hidden, vocabulary, dc_map, collection_id) FROM STDIN") as copy:
        for record in metadata_field_data:
            copy.write_row(record)

def insert_vocabularies(cursor, vocab_data: tuple):
    logging.info('Inserting %s vocabulary rows', len(vocab_data))
    with cursor.copy(
            "COPY vocabulary (vocabulary_id, name, description, vocabulary_type, site_id) FROM STDIN") as copy:
        for record in vocab_data:
            copy.write_row(record)

def insert_vocabulary_terms(cursor, vocab_data: tuple):
    logging.info('Inserting %s vocabulary_list rows', len(vocab_data))
    with cursor.copy(
            "COPY vocabulary_term (vocabulary_id, term) FROM STDIN") as copy:
        for record in vocab_data:
            copy.write_row(record)
