"""
Utilities for extracting data from ContentDM.
"""
import os
import os.path
import logging
from pathlib import Path

"""Module logger."""
logger = logging.getLogger(__name__)

def list_sites() -> list[str]:
    """
    List all available sites.
    :return:
    """
    sites = []
    # List all subdirectories using scandir()
    with os.scandir(f'{os.getenv('ANALYTICS_DATA_PATH')}/sites') as entries:
        for entry in entries:
            if entry.is_dir():
                sites.append(entry.name)
    return sites

def list_collections(site: str) -> list[dict[str,str]]:
    """
    List the collections in a site. Reads a site's collections from the authoritative list in ${site_id}/server/docs/catalog.txt.
    Raw data is extracted without any transformation.

    :param site:
    :return: non-empty list of a site's collections.
    """
    collections = []
    catalog_file = f"{os.getenv('ANALYTICS_DATA_PATH')}/sites/{site}/server/docs/catalog.txt"
    try:
        with open(catalog_file, "r") as file:
            data = file.readlines()
            for line in data:
                props = line.split()

                name = props.pop(0)
                path = props.pop(-1)
                desc = ' '.join(props)

                collections.append({'alias': name, 'description': desc, 'path': path})
    except IOError:
        logger.error('Unable to open %s', catalog_file)

    return collections

def list_metadata_fields(site: str, collection: str) -> list[dict]:
    """
    List metadata fields for a collection. This method extracts the raw data from CDM without any transformations.

    :param site: the site
    :param collection: a collection in the site
    :return: list of metadata fields.
    """

    metadata_fields = []
    logger.info('Listing metadata fields for %s, %s', site, collection)
    config_txt_file = f'{os.getenv('ANALYTICS_DATA_PATH')}/sites/{site}/data/{collection}/index/etc/config.txt'
    try :
        with open (config_txt_file, "r") as file:
            data = file.readlines()
            for line in data:
                vprops = line.strip().split(':')
                metadata_fields.append({
                    'fieldName': vprops[0],
                    'nickname' : vprops[1],
                    'dataType': vprops[2],
                    'largeField': vprops[3],
                    # vprops[4] <-- Not sure what this field is.
                    'required': True if vprops[5] == 'REQ' else False,
                    'searchable': True if vprops[6] == 'SEARCH' else False,
                    'hidden': True if vprops[7] == 'HIDE' else False,
                    'vocabulary': vprops[8],
                    'dcmap': vprops[9]
                })
    except IOError:
        logger.error('Unable to open metadata fields for {site}::{collection}', site, collection)
    return metadata_fields

def is_shared_vocabulary(site: str, collection: str, vocab: str) -> bool:
    """
    Test whether a vocabulary is a "shared" vocabulary.

    :param site:
    :param collection:
    :param vocab:
    :return: boolean flag; true if a shared vocabulary.
    """
    vocab_file = Path(f'{os.getenv('ANALYTICS_DATA_PATH')}/sites/{site}/data/{collection}/index/vocab/{vocab}.map')
    return vocab_file.is_file()

def list_vocabulary_terms(site: str, collection: str, vocab: str) -> list[str]:
    """
    Return a collection vocabulary terms for a collection's vocabulary.

    :param site:
    :param collection:
    :param vocab:
    :return: vocabulary terms for the collection vocabulary.
    """
    vocab_file = f'{os.getenv('ANALYTICS_DATA_PATH')}/sites/{site}/data/{collection}/index/vocab/{vocab}.txt'
    logger.info('Listing vocabulary from %s', vocab_file)

    terms = _read_terms_from_vocabulary_file(vocab_file)
    return terms

def list_authorities(site: str) -> list[dict] :
    """
    Extract authority vocabularies and their terms.

    :param site: site
    """
    vocab_list = []

    oclc_file = f"{os.getenv('ANALYTICS_DATA_PATH')}/sites/{site}/Server/conf/vocab/oclc.txt"
    with open(oclc_file, "r") as oclc_file:
        for line in oclc_file.readlines():
            line = line.strip()
            fields = line.split('\t')
            vocab = fields[0]
            vocab_description = fields[1]

            terms = _read_terms_from_vocabulary_file(f"{os.getenv('ANALYTICS_DATA_PATH')}/sites/{site}/Server/conf/vocab/{vocab}.txt")
            vocab_list.append({'name': vocab, 'description': vocab_description, 'type': 'a', 'terms': terms})
    return vocab_list


def list_shared_vocabularies(site: str) -> list[dict]:
    """
    Extracts all of a site's shared vocabularies and their terms.
    :param site: site id
    :return:
    """
    vocab_list = []

    index_file = f"{os.getenv('ANALYTICS_DATA_PATH')}/sites/{site}/Server/conf/vocab/index.txt"
    with open(index_file, "r") as index_file:
        for line in index_file.readlines():
            line = line.strip()
            fields = line.split()
            vocab = fields[0]
            vocab_description = fields[1]

            logger.debug('Extracting vocabulary terms for shared library \'%s\'', vocab)
            terms = _read_terms_from_vocabulary_file(f"{os.getenv('ANALYTICS_DATA_PATH')}/sites/{site}/Server/conf/vocab/{vocab}.txt")
            vocab_list.append({'name': vocab, 'description': vocab_description, 'type': 's', 'terms': terms})

    return vocab_list


def _read_terms_from_vocabulary_file(f: str) -> list[str]:
    """
    Read the vocabulary terms from a vocabulary file.

    :param f: file path to read
    """
    try :
        with open(f) as vocab_file:
            terms = []
            lines = vocab_file.readlines()
            for line in lines:
                line = line.strip()
                fields = line.split('\t')
                terms.append(fields[0])
            return terms
    except IOError:
        logger.error('Unable to open vocabulary list in %s', vocab_file)